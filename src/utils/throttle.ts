export default function throttle(fn:(...args:Array<any>)=>any,delay=200){
    let flag = true
    return function(this:any,...args:Array<any>){
        if (!flag)return
        flag = false
        setTimeout(()=>{
            fn.apply(this,args)
            flag = true
        },delay)
    }
}
