import axios from "axios";
import {addPending, removePending} from '@/utils/cancelRequest'
import httpErrorStatusHandle from "@/utils/handleError";
import {Toast} from "antd-mobile";
import GenericCreator from "@/utils/genericCreator";
import AxiosRetry from "@/utils/axiosRetry";
const retryInstance = new GenericCreator().create(AxiosRetry)
const axiosInstance = axios.create({
    timeout: 5000,
    baseURL: '/m1/1406756-0-default',
    countLimit: 2,
    waitTime: 1000

})
axiosInstance.interceptors.request.use((config) => {
    removePending(config)
    addPending(config)
    Toast.show({
        icon: 'loading',
        content: '加载中…',
        maskClickable: false
    })
    return config
}, (error) => Promise.reject(error))
axiosInstance.interceptors.response.use((response) => {
    removePending(response.config)
    Toast.clear()
    return response
}, (error) => {
    removePending(error.config)
    const message = httpErrorStatusHandle(error)
    if (axios.isCancel(error)) {
        console.log('repeated request: ' + error.message)
    } else {
        console.log(message)
    }
    return retryInstance.retry(axiosInstance, error)
})
export default axiosInstance
