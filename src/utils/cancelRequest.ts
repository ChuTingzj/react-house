import type {AxiosRequestConfig} from 'axios'
import qs from 'qs'

const pending = new Map()
const addPending = (config: AxiosRequestConfig) => {
    const {url, method, params, data} = config
    const entireUrl = [url, method, qs.stringify(params), qs.stringify(data)].join('&')
    const controller = new AbortController()
    config.signal = config.signal ?? controller.signal
    if (!pending.get(entireUrl)) {
        pending.set(entireUrl, controller)
    }
}
const removePending = (config: AxiosRequestConfig) => {
    const {url, method, params, data} = config
    const entireUrl = [url, method, qs.stringify(params), qs.stringify(data)].join('&')
    if (pending.has(entireUrl)) {
        const controller = pending.get(entireUrl) as AbortController
        controller.abort()
        pending.delete(entireUrl)
    }
}
const clearPending = () => {
    for (const [controller] of pending) {
        (controller as AbortController).abort()
    }
    pending.clear()
}
export {addPending, removePending, clearPending}
