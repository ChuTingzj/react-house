import type {AxiosError, AxiosInstance} from 'axios'

export default class AxiosRetry {
    retry(instance: AxiosInstance, error: AxiosError) {
        const config = error.config
        config.__retryCount = config.__retryCount || 0
        if (config.__retryCount >= config.countLimit) {
            return Promise.reject(error)
        }
        config.__retryCount += 1
        return this.delay(config.waitTime).then(() => instance(error.config))
    }

    delay(waitTime: number) {
        return new Promise(resolve => {
            setTimeout(resolve, waitTime)
        })
    }
}
