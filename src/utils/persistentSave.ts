export default class PersistentSave{
    private storage:Storage
    constructor() {
        this.storage = window.localStorage
    }
    get(key:string){
        return JSON.parse(this.storage.getItem(key)??'{}')
    }
    set(key:string,val:any){
        return this.storage.setItem(key,JSON.stringify(val))
    }
    del(key:string){
        return this.storage.removeItem(key)
    }
    clear(){
        this.storage.clear()
    }
}
