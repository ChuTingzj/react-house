export default class GenericCreator{
    create<T>(c:{new ():T}):T{
        return new c()
    }
}
