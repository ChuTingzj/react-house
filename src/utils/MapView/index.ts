import {IMap} from "@/types";
import {IHouseItem} from '@/types/map'
import {getHouseDataAsync} from '@/features/map/mapSlice'
import {store} from '@/app/store'
export default function getHouse(map: IMap, houseList: IHouseItem[]) {
    houseList.forEach(item => {
        const position = new window.BMapGL.Point(Number(item.coord.longitude), Number(item.coord.latitude))
        const label = new window.BMapGL.Label('', {
            position,
            offset: new window.BMapGL.Size(-35, -35)
        })
        map?.addOverlay(label)
        label.addEventListener('click', () => {
            const {nextZoom} =  getZoom(map)
            //TODO:放大地图
            map.centerAndZoom(position, nextZoom)
            map.clearOverlays()
            const dispatch = store.dispatch
            dispatch(getHouseDataAsync(item.value))
            const houseList = store.getState().map.houseList
            getHouse(map,houseList)
        })
        label.setContent(`
                    <div class="bubble">
                        <div class="name">
                            <p>${item.label}</p>
                            <p>${item.count}</p>
                        </div>  
                    </div>
                `)
        label.setStyle({
            border: '0px solid rgba(255,0,0)',
            padding: '0px',
            whiteSpace: 'nowrap',
            fontSize: '12px',
            color: 'rgb(255,255,255)',
            textAlign: 'center'
        })
    })
}

function getZoom(map: IMap) {
    const zoom = map?.getZoom()
    let nextZoom: number
    let type: string
    if (zoom >= 10 && zoom < 12) {
        nextZoom = 13
        type = 'circle'
    } else if (zoom >= 12 && zoom < 14) {
        nextZoom = 15
        type = 'circle'
    } else {
        nextZoom = 15
        type = 'rect'
    }
    return {
        nextZoom,
        type
    }
}

