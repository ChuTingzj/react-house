interface ICityItem{
    label:string,
    value:string
}
interface IPersonalCityInfo{
    center:{
        lng:number,
        lat:number
    },
    code:number,
    level:number,
    name:string
}
interface ICityList extends ICityItem{
    pinyin:string,
    short:string
}
type CityMap = Object<string,Array<ICityItem|IPersonalCityInfo>>
export type {ICityItem,IPersonalCityInfo,ICityList,CityMap}
