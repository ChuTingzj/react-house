interface INewsItem{
    id:number,
    title:string,
    imgSrc:string,
    from:string,
    date:string
}
export type {INewsItem}
