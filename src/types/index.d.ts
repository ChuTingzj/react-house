import type {IPersonalCityInfo} from './city'
declare global {
    interface Window {
        BMapGL: IMap
    }
}
declare module 'axios' {
    export interface AxiosRequestConfig {
        __retryCount?: number,
        countLimit: number;
        waitTime: number;
    }
}
interface IMap {
    Map: new (container: string) => any,
    Point: new (longitude: number, latitude: number) => any,
    LocalCity: new () => ICity | null,
    Geocoder: new () => IGeo,
    centerAndZoom: (point: any, num: number) => any,
    addOverlay: (param: string | ILabel) => any,
    Marker: new (point: string) => any,
    ZoomControl: new () => any,
    addControl: (param: any) => any,
    ScaleControl: new () => any,
    Label: new (param: string, options: IOption) => ILabel,
    Size: new (x: number, y: number) => any,
    clearOverlays:()=>void,
    getZoom:()=>number
}

interface ICity {
    get: (callback: (param: IPersonalCityInfo) => void) => any
}

interface IGeo {
    getPoint: (location: string, callback: (point: IMap['Point']) => any, city: string) => any
}

interface IOption {
    position?: IMap['Point'],
    offset?: IMap['Size']
}

interface ILabel {
    setStyle: (options: Object<string, string>) => any,
    setContent: (structure: string) => any,
    addEventListener: (event: string, callback: () => any) => any,
    id:string
}


interface ISwiperItem {
    id: number,
    imgSrc: string,
    alt: string
}

interface INavItem {
    src: any,
    text: string,
    id: number,
    path: string
}

interface IGroupItem {
    id: number,
    title: string,
    desc: string,
    imgSrc: string
}

export type {ISwiperItem, INavItem, IGroupItem,IGeo,IMap}
