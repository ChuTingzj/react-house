interface IHouseItem{
    label:string,
    value:string,
    coord:{
        latitude:string,
        longitude:string
    },
    count:number
}
enum EShape {
    circle="circle",
    rect="rect"
}
export type {IHouseItem,EShape}
