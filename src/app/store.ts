import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import swiperReducer from '../features/swiper/swiperSlice';
import groupReducer from "@/features/group/groupSlice";
import newsReducer from "@/features/news/NewsSlice";
import citySReducer from "@/features/city/citySlice";
import mapReducer from "@/features/map/mapSlice";
export const store = configureStore({
  reducer: {
    swiper: swiperReducer,
    group:groupReducer,
    news:newsReducer,
    city:citySReducer,
    map:mapReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
