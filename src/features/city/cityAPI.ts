import request from "@/utils/request";
export const getCityInfo = (name:string)=>request(`/area/info?name=${name}`)
export const getCityList = (level:number)=>request(`/area/city?level=${level}`)
export const getHotList = ()=>request('/area/hot')
