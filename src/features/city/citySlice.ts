import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import type {ICityItem, ICityList, CityMap} from '@/types/city'
import type {RootState} from '@/app/store'
import {getCityInfo, getCityList, getHotList} from './cityAPI'
import GenericCreator from "@/utils/genericCreator";
import PersistentSave from "@/utils/persistentSave";
interface cityState {
    cityInfo: ICityItem,
    cityName: string,
    cityList: ICityList[],
    hotList: ICityList[],
    cityMap: CityMap,
    cityKey: Array<string>,
}
const storage = new GenericCreator().create(PersistentSave)
const initialState: cityState = {
    cityInfo: {value: '', label: ''},
    cityName: '',
    cityList: [],
    hotList: [],
    cityMap: {},
    cityKey: [],
}
const getCityInfoAsync = createAsyncThunk('city/getCityInfo', async (name: string) => {
    const {data: res} = await getCityInfo(name)
    return res.body
})
const getCityListAsync = createAsyncThunk('city/getCityList', async (level: number) => {
    const {data: res} = await getCityList(level)
    return res.body
})
const getHotListAsync = createAsyncThunk('city/getHotList', async () => {
    const {data: res} = await getHotList()
    return res.body
})
const citySlice = createSlice({
    name: 'city',
    initialState,
    reducers: {
        changeName: (state, action) => {
            state.cityName = action.payload
        },
        changeCityMap: (state) => {
            const map: any = {}
            const localCity = storage.get('localCity')
            map['热门城市'] = state.hotList
            map['当前城市'] = localCity
            state.cityList.forEach(item => {
                const first = item.short.slice(0, 1)
                if (!map[first]) {
                    map[first] = [item]
                } else {
                    map[first].push(item)
                }
            })
            state.cityMap = map
        },
        changeCityKey: (state) => {
            const back = Reflect.ownKeys(state.cityMap).slice(2)
            back.sort().unshift('当前城市', '热门城市')
            state.cityKey = (back as Array<string>)
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getCityInfoAsync.fulfilled, (state, action) => {
            state.cityInfo = action.payload
        })
            .addCase(getCityListAsync.fulfilled, (state, action) => {
                state.cityList = action.payload
            })
            .addCase(getHotListAsync.fulfilled, (state, action) => {
                state.hotList = action.payload
            })

    }
})
export default citySlice.reducer
export {getCityInfoAsync, getCityListAsync, getHotListAsync}
export const map = (state: RootState) => state.city
export const {changeName, changeCityMap, changeCityKey} = citySlice.actions
