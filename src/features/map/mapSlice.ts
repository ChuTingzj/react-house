import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import {getHouseData} from './mapAPI'
import type {RootState} from '@/app/store'
import type {IHouseItem} from '@/types/map'
interface mapState {
    houseList:IHouseItem[]
}
const getHouseDataAsync = createAsyncThunk('map/getHouseData',async (value:string)=>{
    const {data:res} = await getHouseData(value)
    return res.body
})
const initialState:mapState = {
    houseList:[]
}
const mapSlice = createSlice({
    name: 'map',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getHouseDataAsync.fulfilled,(state, action)=>{
            state.houseList = action.payload
        })
    }
})
export default mapSlice.reducer
export {getHouseDataAsync}
export const map = (state:RootState)=>state.map
