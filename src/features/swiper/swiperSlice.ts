import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {RootState} from '@/app/store';
import {getSwiperList} from './swiperAPI'
import type {ISwiperItem} from '@/types'
export interface CounterState {
    swiper:ISwiperItem[]
}

const initialState: CounterState = {
    swiper:[]
};
export const getSwiperListAsync = createAsyncThunk(
    'swiper/getSwiperList', async () => {
        const {data:res} = await getSwiperList()
        return res.body
    }
)
export const swiperSlice = createSlice({
    name: 'swiper',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getSwiperListAsync.fulfilled,(state, action)=>{
            state.swiper = action.payload
        })
    },
});
export const swiperList = (state: RootState) => state.swiper;
export default swiperSlice.reducer;
