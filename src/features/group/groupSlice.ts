import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import {getGroupList} from './groupAPI'
import type {IGroupItem} from '@/types'
import type {RootState} from '@/app/store'
interface GroupState {
    groupList: IGroupItem[]
}

const initialState: GroupState = {
    groupList: []
}
const getGroupListAsync = createAsyncThunk('group/getGroupList', async () => {
    const {data: res} = await getGroupList()
    return res.body
})
const groupSlice = createSlice({
    name: 'group',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getGroupListAsync.fulfilled, (state, action) => {
            state.groupList = action.payload
        })
    }
})
export default groupSlice.reducer
export {getGroupListAsync}
export const group = (state:RootState)=>state.group
