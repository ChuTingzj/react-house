import {createSlice,createAsyncThunk} from '@reduxjs/toolkit'
import type {INewsItem} from '@/types/news'
import type {RootState} from '@/app/store'
import {getNewsList} from './NewsAPI'
interface NewsState{
    newsList:INewsItem[]
}
const initialState:NewsState = {
    newsList:[]
}
const getNewsListAsync = createAsyncThunk('news/getNewsList',async ()=>{
    const {data:res} =  await getNewsList()
    return res.body
})
const NewsSlice = createSlice({
    name:'news',
    initialState,
    reducers:{},
    extraReducers:(builder)=>{
        builder.addCase(getNewsListAsync.fulfilled,(state, action)=>{
            state.newsList = action.payload
        })
    }
})
export default NewsSlice.reducer
export {getNewsListAsync}
export const news = (state:RootState)=>state.news
