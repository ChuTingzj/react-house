import {Routes,Route,Navigate,BrowserRouter} from 'react-router-dom'
import {Suspense} from 'react'
import {HomeView,CityList,Index,News,List,Profile,Map,MapView} from './pages'
export default function RouterView(){
    return (
        <BrowserRouter>
            <Suspense>
                <Routes>
                    <Route path={'/'} element={<Navigate to={'/home'}></Navigate>}></Route>
                    <Route path={'/home'} element={<HomeView></HomeView>}>
                        <Route index element={<Index></Index>}></Route>
                        <Route path={'index'} element={<Index></Index>}></Route>
                        <Route path={'news'} element={<News></News>}></Route>
                        <Route path={'list'} element={<List></List>}></Route>
                        <Route path={'profile'} element={<Profile></Profile>}></Route>
                        <Route path={'map'} element={<Map></Map>}></Route>
                    </Route>
                    <Route path={'/citylist'} element={<CityList></CityList>}></Route>
                    <Route path={'/map'} element={<MapView></MapView>}></Route>
                </Routes>
            </Suspense>
        </BrowserRouter>

    )
}
