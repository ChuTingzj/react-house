import {lazy} from 'react'

const HomeView = lazy(() => import('@/views/HomeView/HomeView'))
const CityList = lazy(() => import('@/views/CityList/CityList'))
const Index = lazy(()=>import('@/views/HomeView/Children/index'))
const News = lazy(()=>import('@/views/HomeView/Children/News'))
const List = lazy(()=>import('@/views/HomeView/Children/List'))
const Profile = lazy(()=>import('@/views/HomeView/Children/Profile'))
const Map = lazy(()=>import('@/views/HomeView/Children/Map'))
const MapView = lazy(()=>import('@/views/MapView/MapView'))
export {HomeView, CityList,Index,News,List,Profile,Map,MapView}
