import type {INavItem} from '@/types'
import {ListUl, ListH2} from '@/styles/components/Nav'
import {useNavigate} from 'react-router-dom'

interface Prop {
    data: INavItem[]
}

export default function NavBar(props: Prop) {
    const navigate = useNavigate()
    return (
        <ListUl>
            {
                props.data.map((item) => (
                    <li key={item.id}>
                        <img src={item.src} alt={''} style={{width: 48}} onClick={() => navigate(item.path)}/>
                        <ListH2>{item.text}</ListH2>
                    </li>
                ))
            }
        </ListUl>
    )
}
