import '@/styles/scss/News.css'
import type {INewsItem} from '@/types/news'

interface Prop {
    newsList: INewsItem[]
}

export default function LatestNews(props: Prop) {
    return (
        <div className="news">
            <div style={{display:'flex',justifyContent:'space-between'}}>
                <h3 className="group-title">最新资讯</h3>
            </div>
            <div>
                {
                    props.newsList.length > 0 ? props.newsList.map(item => (
                        <div className="news-item" key={item.id}>
                            <div className="imgwrap">
                                <img
                                    className="img"
                                    src={item.imgSrc}
                                    alt=""
                                />
                            </div>
                            <div className="content">
                                <h3 className="title">{item.title}</h3>
                                <div className="info">
                                    <span>{item.from}</span>
                                    <span>{item.date}</span>
                                </div>
                            </div>
                        </div>
                    )) : ''
                }
            </div>
        </div>
    )
}
