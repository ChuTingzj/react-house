import React, {ReactElement, Suspense} from "react";

export default function LazyLoad(component: string | ReactElement) {
    if (typeof component === 'string') {
        const Com = React.lazy(() => import(component))
        return (
            <Suspense>
                <Com></Com>
            </Suspense>
        )
    } else {
        return (
            <Suspense>
                {component}
            </Suspense>
        )
    }

}
