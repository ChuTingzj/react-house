import React, {forwardRef, ForwardedRef} from "react";
import {useNavigate} from 'react-router-dom'
import {VariableSizeList, VariableSizeList as List} from 'react-window'
import AutoSizer from 'react-virtualized-auto-sizer'
import {useAppDispatch} from '@/hooks/useStoreData'
import {changeName} from '@/features/city/citySlice'
import type {CityMap, ICityList} from '@/types/city'
import GenericCreator from "@/utils/genericCreator";
import PersistentSave from "@/utils/persistentSave";

interface Prop {
    getItemSize: (...args: Array<any>) => number,
    cityKey: Array<string>,
    cityMap: CityMap,
    onItemsRendered: React.Dispatch<React.SetStateAction<number>>
}

function VirtualList({getItemSize, cityKey, cityMap, onItemsRendered}: Prop, ref: ForwardedRef<VariableSizeList>) {
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const storage = new GenericCreator().create(PersistentSave)
    const changeCity = (cityInfo?: ICityList) => {
        if (cityInfo) {
            dispatch(changeName(''))
            storage.del('localCity')
            dispatch(changeName(cityInfo.label))
            storage.set('localCity', cityInfo)
        }
        navigate(-1)
    }
    return (
        <AutoSizer>
            {({height, width}) => (
                <List
                    ref={ref}
                    onItemsRendered={(rows) => onItemsRendered(rows.visibleStartIndex)}
                    layout={'vertical'} itemSize={getItemSize}
                    height={height} itemCount={cityKey.length}
                    width={width}>
                    {({index, style}) => (
                        <div className='outerBox' style={style}>
                            <div className='cityList'>
                                <div className='title'>{cityKey[index]?.toUpperCase()}</div>
                                {
                                    Array.isArray((cityMap as any)[cityKey[index]]) ?
                                        (cityMap as any)[cityKey[index]].map((self: ICityList, i: number) => (
                                            <div onClick={() => changeCity(self)} className='name'
                                                 key={i}>{self?.label}</div>
                                        )) : <div onClick={() => changeCity()}
                                                  className='name'>{cityMap[cityKey[index]]?.name ?? cityMap[cityKey[index]]?.label}</div>
                                }
                            </div>
                        </div>
                    )}
                </List>
            )}
        </AutoSizer>
    )
}

export default forwardRef(VirtualList)
