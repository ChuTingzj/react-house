import {Repositioning, Search, MapDraw} from '@icon-park/react'
import {Container, Box, FontDiv, InnerBox, RightDiv,LeftDiv} from '@/styles/components/SearchBar'
import {useNavigate} from 'react-router-dom'
import {useEffect} from 'react'
import {useAppDispatch, useAppSelector} from '@/hooks/useStoreData'
import {getCityInfoAsync, map, changeName} from '@/features/city/citySlice'
import GenericCreator from "@/utils/genericCreator";
import PersistentSave from "@/utils/persistentSave";
export default function SearchBar() {
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const { cityName} = useAppSelector(map)
    const storage = new GenericCreator().create(PersistentSave)
    useEffect(() => {
        const localCity = storage.get('localCity')
        if (storage.get('localCity')){
            dispatch(changeName(localCity.name??localCity.label))
            return
        }
        let city = new window.BMapGL.LocalCity()
        city!.get((res) => {
            dispatch(changeName(res.name))
            if (!Reflect.has(localCity,'name')&&!Reflect.has(localCity,'label')){
                localStorage.setItem('localCity',JSON.stringify(res))
            }
        })

        dispatch(getCityInfoAsync(cityName))
        return () => {
            city = null
        }
    }, []);

    return (
        <Container>
            <Box>
                {/* 左侧白色区域 */}
                <InnerBox>
                    {/* 位置 */}
                    <LeftDiv>
                        <FontDiv color={''} position='left' onClick={()=>navigate('/citylist')}>{cityName.slice(0,2) ?? '全国'}</FontDiv>
                        <Repositioning theme="outline" size="20" fill="#7f7f80"/>
                    </LeftDiv>

                    {/* 搜索表单 */}
                    <RightDiv>
                        <Search style={{float: 'left', paddingLeft: '3px'}} theme="outline" size="20" fill="#9c9fa1"/>
                        <FontDiv color={'#9c9fa1'} position={''}>搜索小区或地址</FontDiv>
                    </RightDiv>
                </InnerBox>
                {/* 右侧地图图标 */}
                <MapDraw theme="outline" size="20" fill="#fff" onClick={() => navigate('/map')}/>
            </Box>
        </Container>
    )
}
