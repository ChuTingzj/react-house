import {Grid} from 'antd-mobile'
import type {IGroupItem} from '@/types'
import {ContentContainer, ContentBox, TitleBox} from '@/styles/components/HouseGroup'

interface Prop {
    data: IGroupItem[]
}
export default function HouseGroup(props: Prop) {
    return (
        <div style={{padding: '10px'}}>
            <TitleBox>
                <h3>租房小组</h3>
                <span>更多</span>
            </TitleBox>
            <Grid columns={2} gap={10}>

                {
                    props.data.map(item =>
                        <Grid.Item key={item.id}>
                            <ContentContainer>
                                <ContentBox>
                                    <b>{item.title}</b>
                                    <span>{item.desc}</span>
                                </ContentBox>
                                <img src={item.imgSrc} alt="" style={{width: '70px'}}/>
                            </ContentContainer>
                        </Grid.Item>
                    )
                }

            </Grid>
        </div>
    )
}
