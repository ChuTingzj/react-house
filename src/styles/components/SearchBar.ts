import styled from "styled-components";

const Container = styled.div`
  position: absolute;
  top: 7vw;
  z-index: 999;
  left: 5vw;
  width: 90vw
`
const Box = styled.div`
  display: flex;
  justify-content: center;
`
const InnerBox = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
  width:77%;
  background-color: #fff;
  border-radius: 30px
`
const LeftDiv = styled.div`
  display: flex;
  align-items: center;
  margin-left: 1vw;
`
const RightDiv = styled.div`
  display: flex;
  align-items: center;
  border-left: 1px solid #333;
  margin-left: 1vw;
  width: 33.333vw
`
const FontDiv = styled.div`
  font-size: 13px;
  padding-top: 0.3vw;
  color: ${(props: { color: string, position: string }) => props.color};
  float: ${(props) => props.position};
`
export {Container, Box, FontDiv, InnerBox, RightDiv,LeftDiv}
