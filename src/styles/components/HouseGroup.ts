import styled from "styled-components";
const TitleBox = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 4vw
`
const ContentContainer = styled.div`
  margin-top: 4vw;
  height: 20vw;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #fff
`
const ContentBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around
`
export {TitleBox,ContentBox,ContentContainer}
