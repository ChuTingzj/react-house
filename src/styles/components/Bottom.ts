import styled from 'styled-components'

const Bottom = styled.div`
  position: sticky;
  bottom: 0;
  background-color: #ffffff;
`
export {Bottom}
