import styled from 'styled-components'

const Parent = styled.div`
  background-color: #ffffff;
  grid-row-start: 1;
`
const ListUl = styled.ul`
  display:flex;
  justify-content:space-between;
`
const ListH2 = styled.h2`
  font-size:13px;
  font-weight:400;
  text-align:center
`
export {Parent,ListUl,ListH2}
