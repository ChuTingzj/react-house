import {useRef,useEffect,useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {NavBar} from 'antd-mobile'
import {useAppDispatch, useAppSelector} from '@/hooks/useStoreData'
import {
    getCityListAsync,
    map,
    getHotListAsync,
    changeCityMap,
    changeCityKey
} from '@/features/city/citySlice'
import type {VariableSizeList} from 'react-window'
import VirtualList from "@/components/VirtualList";
import '@/styles/scss/CityList.scss'
function isArray(param: any) {
    if (Array.isArray(param)) return param?.length * 40
    return 40
}
function replaceText(text: string) {
    if (text === '当') return '#'
    return '★'
}
export default function CityList() {
    const list = useRef<VariableSizeList>(null);
    const navigate = useNavigate()
    const [activeIndex,setActiveIndex] = useState(0)
    const dispatch = useAppDispatch()
    const {cityMap, cityKey} = useAppSelector(map)
    const getItemSize = (index: number) => {
        return 50 + isArray(cityMap[cityKey[index]])
    };
    useEffect(() => {
        Promise.allSettled([dispatch(getCityListAsync(1)), dispatch(getHotListAsync())])
            .then(() => {
                dispatch(changeCityMap())
                dispatch(changeCityKey())
            })
            .catch(error => console.log(error))
    }, [])

    return (
        <div className='container'>
            <NavBar style={{backgroundColor: '#fff', marginTop: '-12vw'}} back='返回' onBack={() => navigate(-1)}
                    children={'城市选择'}></NavBar>
            <VirtualList ref={list} onItemsRendered={setActiveIndex} getItemSize={getItemSize} cityKey={cityKey} cityMap={cityMap}></VirtualList>
            <div className="indexList">
                <ul className='city-index'>
                    {
                        cityKey.map((item, index) => (
                            <li key={item} className="city-index-item">
                                <div className={activeIndex === index ? 'index-active' : 'index-normal'}>
                                    <span onClick={()=>{
                                        list?.current?.scrollToItem(index)
                                    }}>{item.length > 1 ? replaceText(item.slice(0, 1)) : item.toUpperCase()}</span>
                                </div>
                            </li>
                        ))
                    }
                </ul>
            </div>
        </div>
    )
}
