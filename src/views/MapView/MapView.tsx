import {useEffect} from 'react'
import {NavBar} from "antd-mobile";
import {useNavigate} from 'react-router-dom'
import GenericCreator from "@/utils/genericCreator";
import PersistentSave from "@/utils/persistentSave";
import {useSelector, useDispatch} from 'react-redux'
import type {AppDispatch} from '@/app/store'
import {getHouseDataAsync, map} from '@/features/map/mapSlice'
import getHouse from "@/utils/MapView";
import '@/styles/scss/MapView.scss'

export default function MapView() {
    const storage = new GenericCreator().create(PersistentSave)
    const dispatch: AppDispatch = useDispatch()
    const {houseList} = useSelector(map)
    const navigate = useNavigate()
    const cityName = storage.get('localCity').label ?? storage.get('localCity').name
    const value = storage.get('localCity').value
    useEffect(() => {
        let map = new window.BMapGL.Map("container")
        const mapGeo = new window.BMapGL.Geocoder()
        dispatch(getHouseDataAsync(value))
        mapGeo.getPoint(cityName, (point) => {
            if (point) {
                map?.centerAndZoom(point, 11)
                map?.addControl(new window.BMapGL.ZoomControl())
                map?.addControl(new window.BMapGL.ScaleControl())
                getHouse(map,houseList)
            }
        }, cityName)
        return () => {
            map = null
        };
    }, []);
    return (
        <div className={'map'}>
            <NavBar back='返回' onBack={() => navigate(-1)}>地图找房</NavBar>
            <div id="container"></div>
        </div>
    )
}
