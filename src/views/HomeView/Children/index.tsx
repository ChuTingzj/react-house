import {Swiper} from "antd-mobile";
import {useEffect} from 'react'
import {useAppDispatch, useAppSelector} from '@/hooks/useStoreData'
import {getSwiperListAsync, swiperList} from '@/features/swiper/swiperSlice'
import {getGroupListAsync, group} from '@/features/group/groupSlice'
import {news, getNewsListAsync} from '@/features/news/NewsSlice'
import NavBar from '@/components/NavBar'
import HouseGroup from "@/components/HouseGroup";
import Nav1 from '@/assets/nav/nav-1.png'
import Nav2 from '@/assets/nav/nav-2.png'
import Nav3 from '@/assets/nav/nav-3.png'
import Nav4 from '@/assets/nav/nav-4.png'
import {Parent} from '@/styles/components/Nav'
import {Container} from '@/styles/components/Header'
import SearchBar from '@/components/SearchBar'
import LatestNews from "@/components/LatestNews";

const data = [{src: Nav1, text: '整租', id: 1, path: '/home/list'}, {
    src: Nav2,
    text: '合租',
    id: 2,
    path: '/home/list'
}, {src: Nav3, text: '地图找房', id: 3, path: '/home/city'}, {src: Nav4, text: '去出租', id: 4, path: '/home/list'}]
export default function Index() {
    const dispatch = useAppDispatch()
    const {swiper} = useAppSelector(swiperList)
    const {groupList} = useAppSelector(group)
    const {newsList} = useAppSelector(news)
    useEffect(() => {
        Promise.allSettled([dispatch(getSwiperListAsync()), dispatch(getGroupListAsync()), dispatch(getNewsListAsync())]).catch(error => console.log(error))
    }, [])
    const items = swiper.map((item) => (
        <Swiper.Item key={item.id}>
            <img
                src={item.imgSrc}
                alt={item.alt}
                style={{width: '100%', verticalAlign: 'top'}}
            />
        </Swiper.Item>
    ))


    return (
        <div>
            <SearchBar></SearchBar>
            <Container>
                <Parent>
                    {
                        swiper.length > 0 ? <Swiper autoplay loop>{items}</Swiper> : null
                    }
                    <NavBar data={data}></NavBar>
                </Parent>
                <HouseGroup data={groupList}></HouseGroup>
                <LatestNews newsList={newsList}></LatestNews>
            </Container>
        </div>

    )
}
