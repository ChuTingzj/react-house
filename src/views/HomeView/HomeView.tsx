import {Outlet} from 'react-router-dom'
import {TabBar, Badge} from 'antd-mobile'
import {GlobalOutline, SearchOutline, ContentOutline, UserOutline} from 'antd-mobile-icons'
import {useNavigate,useLocation} from 'react-router-dom'
import {Bottom} from '@/styles/components/Bottom'

const tabs = [
    {
        key: '/home/index',
        title: '首页',
        icon: <GlobalOutline/>,
        badge: Badge.dot,
    },
    {
        key: '/home/list',
        title: '找房',
        icon: <SearchOutline/>,
        badge: '5',
    },
    {
        key: '/home/news',
        title: '资讯',
        icon: <ContentOutline/>,
        badge: '99+',
    },
    {
        key: '/home/profile',
        title: '我的',
        icon: <UserOutline/>,
    },
]
export default function HomeView() {
    const navigate = useNavigate()
    const location = useLocation()
    const changeHandler = (key: string) => navigate(`${key}`)
    return (
        <div>
            <div>
                <Outlet></Outlet>
            </div>
            <Bottom>
                <TabBar safeArea activeKey={location.pathname==='/home'?'/home/index':location.pathname} onChange={(key) => changeHandler(key)}>
                    {tabs.map(item => (
                        <TabBar.Item key={item.key} icon={item.icon} title={item.title} badge={item.badge}/>
                    ))}
                </TabBar>
            </Bottom>
        </div>

    )
}
